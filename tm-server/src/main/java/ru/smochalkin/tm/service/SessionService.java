package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.ISessionService;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.UserIsLocked;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.util.HashUtil;

import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(@NotNull ISessionRepository sessionRepository, @NotNull ServiceLocator serviceLocator) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public final Session open(@NotNull final String login, @NotNull final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        if (user.isLock()) throw new UserIsLocked();
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!user.getPasswordHash().equals(hash)) throw new AccessDeniedException();
        @NotNull final Session session = new Session(user.getId());
        repository.add(session);
        return sign(session);
    }

    @Override
    public final void close(@NotNull final Session session) {
        removeById(session.getId());
    }

    @Override
    public final void closeAllByUserId(@Nullable final String userId) {
        List<Session> list = findAllByUserId(userId);
        removeAll(list);
    }

    @Override
    public final void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!repository.existsById(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final @Nullable User user = serviceLocator.getUserService().findById(session.getUserId());
        if (user == null) throw new AccessDeniedException();
        if (!user.getRole().equals(role)) throw new AccessDeniedException();
    }

    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final IPropertyService ps = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(session, ps.getSignSecret(), ps.getSignIteration());
        session.setSignature(signature);
        return session;
    }

    @Override
    @NotNull
    public List<Session> findAllByUserId(@Nullable final String userId) {
        if(userId == null) throw new EmptyIdException();
        return sessionRepository.findAllByUserId(userId);
    }

}