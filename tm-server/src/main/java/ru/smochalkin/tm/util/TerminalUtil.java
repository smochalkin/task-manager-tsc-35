package ru.smochalkin.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextInt() {
        @NotNull final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException(value);
        }
    }

}
