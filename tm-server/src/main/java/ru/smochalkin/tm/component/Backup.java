package ru.smochalkin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.service.IDomainService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Backup {

    private final int interval = 30;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    public final IDomainService domainService;

    public Backup(@NotNull final IDomainService domainService) {
        this.domainService = domainService;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, interval, interval, TimeUnit.SECONDS);
    }

    public void save() {
        domainService.saveBackup();
    }

    public void load() {
        domainService.loadBackup();
    }

}


