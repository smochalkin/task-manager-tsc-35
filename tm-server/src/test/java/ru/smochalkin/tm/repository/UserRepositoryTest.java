package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void init() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();

        for (int i = 0; i < ENTRY_COUNT; i++) {
            @NotNull final User session = new User();
            session.setLogin("user" + i);
            session.setRole(Role.USER);
            userRepository.add(session);
            userList.add(session);
        }
    }

    @Test
    public void addTest() {
        @NotNull final User newUser = new User();
        userRepository.add(newUser);
        Assert.assertEquals(ENTRY_COUNT + 1, userRepository.getCount());
    }

    @Test
    public void addAllTest() {
        @NotNull final int expectedSize = userList.size() + userRepository.getCount();
        userRepository.addAll(userList);
        Assert.assertEquals(expectedSize, userRepository.getCount());
    }

    @Test
    public void clearTest() {
        userRepository.clear();
        Assert.assertEquals(0, userRepository.getCount());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<User> actualUserList = userRepository.findAll();
        Assert.assertEquals(userList, actualUserList);
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> userRepository.findById(UUID.randomUUID().toString()));
        for (@NotNull final User session : userList) {
            Assert.assertNotNull(userRepository.findById(session.getId()));
            Assert.assertEquals(session, userRepository.findById(session.getId()));
        }
    }

    @Test
    public void existsByIdTest() {
        @NotNull final String isId = userList.get(0).getId();
        @NotNull final String notId = UUID.randomUUID().toString();
        Assert.assertTrue(userRepository.existsById(isId));
        Assert.assertFalse(userRepository.existsById(notId));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(ENTRY_COUNT, userRepository.getCount());
    }

    @Test
    public void removeTest() {
        for (@NotNull final User session : userList) {
            userRepository.remove(session);
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> userRepository.findById(session.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        for (@NotNull final User session : userList) {
            Assert.assertNotNull(userRepository.removeById(session.getId()));
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> userRepository.findById(session.getId()));
        }
    }

    @Test
    public void findByLoginTest() {
        Assert.assertThrows(UserNotFoundException.class,
                () -> userRepository.findByLogin("not_login"));
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userRepository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void removeByLoginTest() {
        Assert.assertThrows(UserNotFoundException.class,
                () -> userRepository.removeByLogin("not_login"));
        for (@NotNull final User user : userList) {
            userRepository.removeByLogin(user.getLogin());
            Assert.assertThrows(UserNotFoundException.class,
                    () -> userRepository.findByLogin(user.getLogin()));
        }
        Assert.assertEquals(0, userRepository.getCount());
    }

    @Test
    public void isLoginTest() {
        @NotNull final String isLogin = userList.get(0).getLogin();
        @NotNull final String notLogin = UUID.randomUUID().toString();
        Assert.assertTrue(userRepository.isLogin(isLogin));
        Assert.assertFalse(userRepository.isLogin(notLogin));
    }

}
