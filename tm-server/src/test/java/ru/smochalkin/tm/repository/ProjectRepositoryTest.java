package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void init() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();

        for (int i = 0; i < ENTRY_COUNT; i++) {
            @NotNull final Project project = new Project();
            project.setName("name " + i);
            project.setDescription("description " + i);
            if ((i < ENTRY_COUNT / 2))
                project.setUserId(USER_ID_1);
            else
                project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void addTest() {
        @NotNull final Project newProject = new Project();
        projectRepository.add(newProject);
        Assert.assertEquals(ENTRY_COUNT + 1, projectRepository.getCount());
    }

    @Test
    public void addAllTest() {
        @NotNull final int expectedSize = projectList.size() + projectRepository.getCount();
        projectRepository.addAll(projectList);
        Assert.assertEquals(expectedSize, projectRepository.getCount());
    }

    @Test
    public void clearTest() {
        projectRepository.clear();
        Assert.assertEquals(0, projectRepository.getCount());
    }


    @Test
    public void clearByUserTest() {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.clear(USER_ID_1);
        Assert.assertEquals(0, projectRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        Assert.assertEquals(projectList, actualProjectList);
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Project> projects = projectRepository.findAll(USER_ID_1);
        Assert.assertEquals(ENTRY_COUNT / 2, projects.size());
        Assert.assertEquals(USER_ID_1, projects.get(0).getUserId());
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findById(UUID.randomUUID().toString()));
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.findById(project.getId()));
            Assert.assertEquals(project, projectRepository.findById(project.getId()));
        }
    }


    @Test
    public void findByIdAndByUserIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findById(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Project project : projectList) {
            if (USER_ID_1.equals(project.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            Assert.assertEquals(project, projectRepository.findByName(userId, project.getName()));
        }
    }

    @Test
    public void findByNameTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findByName(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Project project : projectList) {
            if (USER_ID_1.equals(project.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            Assert.assertEquals(project, projectRepository.findByName(userId, project.getName()));
        }
    }

    @Test
    public void findByIndexTest() {
        @NotNull final Project project1 = projectList.get(0);
        @NotNull final Project project2 = projectList.get(ENTRY_COUNT / 2);
        Assert.assertEquals(project1, projectRepository.findByIndex(USER_ID_1, 0));
        Assert.assertEquals(project2, projectRepository.findByIndex(USER_ID_2, 0));
    }

    @Test
    public void existsByIdTest() {
        @NotNull final String isId = projectList.get(0).getId();
        @NotNull final String notId = UUID.randomUUID().toString();
        Assert.assertTrue(projectRepository.existsById(isId));
        Assert.assertFalse(projectRepository.existsById(notId));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(ENTRY_COUNT, projectRepository.getCount());
    }

    @Test
    public void removeTest() {
        for (@NotNull final Project project : projectList) {
            projectRepository.remove(project);
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.removeById(project.getId()));
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void removeByIdAndByUserIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.removeById(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Project project : projectList) {
            if (USER_ID_1.equals(project.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            projectRepository.removeById(userId, project.getId());
            @NotNull final String uId = userId;
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> projectRepository.findById(uId, project.getId()));
        }
    }

    @Test
    public void removeByNameTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.removeByName(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Project project : projectList) {
            if (USER_ID_1.equals(project.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            projectRepository.removeByName(userId, project.getName());
            @NotNull final String uId = userId;
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> projectRepository.findByName(uId, project.getName()));
        }
    }

    @Test
    public void removeByIndexTest() {
        @NotNull final Project project1 = projectList.get(0);
        @NotNull final Project project2 = projectList.get(ENTRY_COUNT / 2);
        projectRepository.removeByIndex(USER_ID_1, 0);
        Assert.assertNotEquals(project1, projectRepository.findByIndex(USER_ID_1, 0));
        projectRepository.removeByIndex(USER_ID_2, 0);
        Assert.assertNotEquals(project2, projectRepository.findByIndex(USER_ID_2, 0));
    }

    @Test
    public void removeAllTest() {
        projectRepository.removeAll(projectList);
        Assert.assertEquals(0, projectRepository.getCount());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final Project project = projectList.get(0);
        @NotNull final String projectId = project.getId();
        @NotNull final String testName = "test name";
        @NotNull final String testDesc = "test desc";
        project.setName(testName);
        project.setDescription(testDesc);
        Assert.assertEquals(testName, project.getName());
        Assert.assertEquals(testDesc, project.getDescription());
        Assert.assertEquals(project, projectRepository.findById(projectId));
    }

    @Test
    public void updateByIdAndByUserIdTest() {
        @NotNull final Project project = projectList.get(0);
        @NotNull final String projectId = project.getId();
        @NotNull final String testName = "test name";
        @NotNull final String testDesc = "test desc";
        project.setName(testName);
        project.setDescription(testDesc);
        Assert.assertEquals(testName, project.getName());
        Assert.assertEquals(testDesc, project.getDescription());
        Assert.assertEquals(project, projectRepository.findById(USER_ID_1, projectId));
    }

    @Test
    public void updateByIndexTest() {
        @NotNull final Project project = projectList.get(0);
        @NotNull final String testName = "test name";
        @NotNull final String testDesc = "test desc";
        project.setName(testName);
        project.setDescription(testDesc);
        Assert.assertEquals(testName, project.getName());
        Assert.assertEquals(testDesc, project.getDescription());
        Assert.assertEquals(project, projectRepository.findByIndex(USER_ID_1, 0));
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final Project project = projectList.get(0);
        @NotNull final String projectId = project.getId();
        @Nullable final Date startDate = project.getStartDate();
        projectRepository.updateStatusById(USER_ID_1, projectId, Status.IN_PROGRESS);
        @NotNull final Project actualProject = projectRepository.findById(projectId);
        Assert.assertEquals(project, actualProject);
        Assert.assertNotEquals(startDate, actualProject.getStartDate());
    }

    @Test
    public void updateStatusByNameTest() {
        @NotNull final Project project = projectList.get(0);
        @NotNull final String projectName = project.getName();
        @Nullable final Date startDate = project.getStartDate();
        projectRepository.updateStatusByName(USER_ID_1, projectName, Status.IN_PROGRESS);
        @NotNull final Project actualProject = projectRepository.findByName(USER_ID_1, projectName);
        Assert.assertEquals(project, actualProject);
        Assert.assertNotEquals(startDate, actualProject.getStartDate());
    }

    @Test
    public void updateStatusByIndexTest() {
        @NotNull final Project project = projectList.get(0);
        @Nullable final Date endDate = project.getEndDate();
        projectRepository.updateStatusByIndex(USER_ID_1, 0, Status.COMPLETED);
        @NotNull final Project actualProject = projectRepository.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(project, actualProject);
        Assert.assertNotEquals(endDate, actualProject.getEndDate());
    }

    @Test
    public void getCountByUserTest() {
        Assert.assertEquals(ENTRY_COUNT / 2, projectRepository.getCountByUser(USER_ID_1));
    }

}
