package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;


    @Before
    public void init() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();

        for (int i = 0; i < ENTRY_COUNT; i++) {
            @NotNull final Session session = new Session();
            if ((i < ENTRY_COUNT / 2))
                session.setUserId(USER_ID_1);
            else
                session.setUserId(USER_ID_2);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void addTest() {
        @NotNull final Session newSession = new Session();
        sessionRepository.add(newSession);
        Assert.assertEquals(ENTRY_COUNT + 1, sessionRepository.getCount());
    }

    @Test
    public void addAllTest() {
        @NotNull final int expectedSize = sessionList.size() + sessionRepository.getCount();
        sessionRepository.addAll(sessionList);
        Assert.assertEquals(expectedSize, sessionRepository.getCount());
    }

    @Test
    public void clearTest() {
        sessionRepository.clear();
        Assert.assertEquals(0, sessionRepository.getCount());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList, actualSessionList);
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> sessionRepository.findById(UUID.randomUUID().toString()));
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.findById(session.getId()));
            Assert.assertEquals(session, sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void existsByIdTest() {
        @NotNull final String isId = sessionList.get(0).getId();
        @NotNull final String notId = UUID.randomUUID().toString();
        Assert.assertTrue(sessionRepository.existsById(isId));
        Assert.assertFalse(sessionRepository.existsById(notId));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(ENTRY_COUNT, sessionRepository.getCount());
    }

    @Test
    public void removeTest() {
        for (@NotNull final Session session : sessionList) {
            sessionRepository.remove(session);
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.removeById(session.getId()));
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> sessionRepository.findById(session.getId()));
        }
    }

    @Test
    public void removeAllTest() {
        sessionRepository.removeAll(sessionList);
        Assert.assertEquals(0, sessionRepository.getCount());
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Session> sessions = sessionRepository.findAllByUserId(USER_ID_1);
        Assert.assertEquals(ENTRY_COUNT / 2, sessions.size());
        Assert.assertEquals(USER_ID_1, sessions.get(0).getUserId());
    }

}
