package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private IUserService userService;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
        @NotNull final User admin = userService.create("admin", "100", Role.ADMIN);
        @NotNull final User user = userService.create("user", "1", "user@user.ru");
        userList = new ArrayList<>();
        userList.add(admin);
        userList.add(user);

    }

    @Test
    public void createTest() {
        Assert.assertEquals(2, userService.getCount());
        userService.create("guest", "10");
        Assert.assertEquals(3, userService.getCount());
    }

    @Test
    public void createWithEmailTest() {
        Assert.assertEquals(2, userService.getCount());
        userService.create("guest", "10", "guest@guest.ru");
        Assert.assertEquals(3, userService.getCount());
    }

    @Test
    public void createWithRoleTest() {
        Assert.assertEquals(2, userService.getCount());
        userService.create("guest", "10", Role.USER);
        Assert.assertEquals(3, userService.getCount());
    }

    @Test
    public void findByLoginTest() {
        Assert.assertEquals(userList.get(0), userService.findByLogin("admin"));
    }

    @Test
    public void removeByLoginTest() {
        Assert.assertThrows(UserNotFoundException.class,
                () -> userService.removeByLogin("not_exists")
        );
        Assert.assertEquals(2, userService.getCount());
        userService.removeByLogin("user");
        Assert.assertEquals(1, userService.getCount());
    }

    @Test
    public void setPasswordTest() {
        @NotNull final String hash = userList.get(0).getPasswordHash();
        userService.setPassword(userList.get(0).getId(), "1000");
        Assert.assertNotEquals(hash, userService.findByLogin("admin").getPasswordHash());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final User expectedUser = userList.get(0);
        @NotNull final String userId = expectedUser.getId();
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        expectedUser.setFirstName(newFirstName);
        expectedUser.setMiddleName(newMiddleName);
        expectedUser.setLastName(newLastName);
        @Nullable final User userActual = userService.updateById(userId, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(expectedUser, userActual);
    }

    @Test
    public void isLoginTest() {
        Assert.assertThrows(EmptyLoginException.class,
                () -> userService.isLogin("")
        );
        Assert.assertTrue(userService.isLogin("admin"));
    }

    @Test
    public void lockUserByLoginTest() {
        Assert.assertFalse(userService.findByLogin("user").isLock());
        userService.lockUserByLogin("user");
        Assert.assertTrue(userService.findByLogin("user").isLock());
    }

    @Test
    public void unlockUserByLoginTest() {
        userService.lockUserByLogin("user");
        Assert.assertTrue(userService.findByLogin("user").isLock());
        userService.unlockUserByLogin("user");
        Assert.assertFalse(userService.findByLogin("user").isLock());
    }

}
