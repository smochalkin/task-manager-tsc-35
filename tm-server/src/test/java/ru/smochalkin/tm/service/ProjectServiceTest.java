package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.repository.ProjectRepository;
import ru.smochalkin.tm.repository.TaskRepository;
import ru.smochalkin.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private IUserService userService;

    @Before
    public void init() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
        projectService = new ProjectService(projectRepository);
        projectList = new ArrayList<>();
        @NotNull final User admin = userService.create("admin", "100", Role.ADMIN);
        @NotNull final User user = userService.create("user", "1", "user@user.ru");
        userService.add(admin);
        userService.add(user);
        @NotNull final Project adminProject = new Project();
        adminProject.setUserId(admin.getId());
        adminProject.setName("admin project");
        adminProject.setDescription("lorem");
        @NotNull final Project userProject = new Project();
        userProject.setUserId(user.getId());
        userProject.setName("user project");
        userProject.setDescription("ipsum");
        projectList.add(adminProject);
        projectList.add(userProject);
        @NotNull final Task adminTask = new Task();
        adminTask.setUserId(admin.getId());
        adminTask.setName("admin task");
        adminTask.setDescription("lorem");
        @NotNull final Task userTask = new Task();
        userTask.setUserId(user.getId());
        userTask.setName("user task");
        userTask.setDescription("ipsum");
        for (@NotNull final Project project : projectList) projectService.add(project);
    }

    @Test
    public void createTest() {
        Assert.assertEquals(2, projectService.getCount());
        projectService.create("1", "2", "3");
        Assert.assertEquals(3, projectService.getCount());
    }

    @Test
    public void clearTest() {
        projectService.clear();
        Assert.assertEquals(0, projectService.getCount());
    }

    @Test
    public void findAllTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull List<Project> projectList = projectService.findAll(userId);
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(projectList.get(0), projectService.findById(userId, projectId));
    }

    @Test
    public void findByNameTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "admin project";
        Assert.assertEquals(projectList.get(0), projectService.findByName(userId, projectName));
    }

    @Test
    public void findByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(projectList.get(0), projectService.findByIndex(userId, 0));
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectService.removeById(userId, projectId);
        Assert.assertEquals(1, projectService.getCount());
    }

    @Test
    public void removeByNameTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "admin project";
        projectService.removeByName(userId, projectName);
        Assert.assertEquals(1, projectService.getCount());
    }

    @Test
    public void removeByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.removeByIndex(userId, 0);
        Assert.assertEquals(1, projectService.getCount());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectService.updateById(userId, projectId, "new name", "new desc");
        Assert.assertEquals("new name", projectList.get(0).getName());
        Assert.assertEquals("new desc", projectList.get(0).getDescription());
    }

    @Test
    public void updateByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.updateByIndex(userId, 0, "new name", "new desc");
        Assert.assertEquals("new name", projectList.get(0).getName());
        Assert.assertEquals("new desc", projectList.get(0).getDescription());
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectService.updateStatusById(userId, projectId, "COMPLETED");
        Assert.assertEquals(Status.COMPLETED, projectList.get(0).getStatus());
    }

    @Test
    public void updateStatusByNameTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectName = "admin project";
        projectService.updateStatusByName(userId, projectName, "COMPLETED");
        Assert.assertEquals(Status.COMPLETED, projectList.get(0).getStatus());
    }

    @Test
    public void updateStatusByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.updateStatusByIndex(userId, 0, "COMPLETED");
        Assert.assertEquals(Status.COMPLETED, projectList.get(0).getStatus());
    }

    @Test
    public void isNotIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertTrue(projectService.isNotIndex(userId, 99));
        Assert.assertFalse(projectService.isNotIndex(userId, 0));
    }

}
