package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.component.Bootstrap;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.repository.SessionRepository;

import java.util.List;

public class SessionServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    @Before
    public void init() {
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final String userId = userService.create("user", "1", "user@user.ru").getId();
        @NotNull final String adminId = userService.create("admin", "100", Role.ADMIN).getId();
        @NotNull final Session adminSession = new Session();
        adminSession.setUserId(adminId);
        sessionService.add(adminSession);
        @NotNull final Session userSession = new Session();
        userSession.setUserId(userId);
        sessionService.add(userSession);
    }

    @Test
    public void openTest() {
        @NotNull final int initialSize = sessionService.getCount();
        @NotNull final Session session = sessionService.open("user", "1");
        Assert.assertEquals(initialSize + 1, sessionService.getCount());
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    public void closeTest() {
        @NotNull final int initialSize = sessionService.getCount();
        @NotNull final Session session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getCount());
    }

    @Test
    public void closeAllByUserIdTest() {
        @NotNull final String userId = sessionService.findAll().get(0).getUserId();
        @NotNull final List<Session> sessions = sessionService.findAllByUserId(userId);
        @NotNull final int resultSize = sessionService.getCount() - sessions.size();
        sessionService.closeAllByUserId(userId);
        Assert.assertEquals(resultSize, sessionService.getCount());
    }

    @Test
    public void validateTest() {
        @NotNull final Session session = sessionService.open("admin", "100");
        sessionService.validate(session);
    }

    @Test
    public void validateRoleTest() {
        @NotNull final Session session = sessionService.open("admin", "100");
        sessionService.validate(session, Role.ADMIN);
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final String userId = sessionService.findAll().get(0).getUserId();
        @NotNull final List<Session> sessions = sessionService.findAllByUserId(userId);
        Assert.assertEquals(userId, sessions.get(0).getUserId());
    }

}
