package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.repository.ProjectRepository;
import ru.smochalkin.tm.repository.TaskRepository;
import ru.smochalkin.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private List<Task> taskList;

    @Before
    public void init() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        @NotNull final User admin = userService.create("admin", "100", Role.ADMIN);
        @NotNull final User user = userService.create("user", "1", "user@user.ru");
        userService.add(admin);
        userService.add(user);
        @NotNull final Project adminProject = new Project();
        adminProject.setUserId(admin.getId());
        adminProject.setName("admin project");
        adminProject.setDescription("lorem");
        @NotNull final Project userProject = new Project();
        userProject.setUserId(user.getId());
        userProject.setName("user project");
        userProject.setDescription("ipsum");
        projectList.add(adminProject);
        projectList.add(userProject);
        @NotNull final Task adminTask = new Task();
        adminTask.setUserId(admin.getId());
        adminTask.setName("admin task");
        adminTask.setDescription("lorem");
        @NotNull final Task userTask = new Task();
        userTask.setUserId(user.getId());
        userTask.setName("user task");
        userTask.setDescription("ipsum");
        taskList.add(adminTask);
        taskList.add(userTask);
        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
        projectTaskService.bindTaskByProjectId(admin.getId(), projectList.get(0).getId(), taskList.get(0).getId());
    }

    @Test
    public void createTest() {
        Assert.assertEquals(2, taskService.getCount());
        taskService.create("1", "2", "3");
        Assert.assertEquals(3, taskService.getCount());
    }

    @Test
    public void clearTest() {
        taskService.clear();
        Assert.assertEquals(0, taskService.getCount());
    }

    @Test
    public void findAllTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull List<Task> taskList = taskService.findAll(userId);
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertEquals(taskList.get(0), taskService.findById(userId, taskId));
    }

    @Test
    public void findByNameTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "admin task";
        Assert.assertEquals(taskList.get(0), taskService.findByName(userId, taskName));
    }

    @Test
    public void findByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(taskList.get(0), taskService.findByIndex(userId, 0));
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        taskService.removeById(userId, taskId);
        Assert.assertEquals(1, taskService.getCount());
    }

    @Test
    public void removeByNameTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "admin task";
        taskService.removeByName(userId, taskName);
        Assert.assertEquals(1, taskService.getCount());
    }

    @Test
    public void removeByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        taskService.removeByIndex(userId, 0);
        Assert.assertEquals(1, taskService.getCount());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        taskService.updateById(userId, taskId, "new name", "new desc");
        Assert.assertEquals("new name", taskList.get(0).getName());
        Assert.assertEquals("new desc", taskList.get(0).getDescription());
    }

    @Test
    public void updateByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        taskService.updateByIndex(userId, 0, "new name", "new desc");
        Assert.assertEquals("new name", taskList.get(0).getName());
        Assert.assertEquals("new desc", taskList.get(0).getDescription());
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        taskService.updateStatusById(userId, taskId, "COMPLETED");
        Assert.assertEquals(Status.COMPLETED, taskList.get(0).getStatus());
    }

    @Test
    public void updateStatusByNameTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "admin task";
        taskService.updateStatusByName(userId, taskName, "COMPLETED");
        Assert.assertEquals(Status.COMPLETED, taskList.get(0).getStatus());
    }

    @Test
    public void updateStatusByIndexTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        taskService.updateStatusByIndex(userId, 0, "COMPLETED");
        Assert.assertEquals(Status.COMPLETED, taskList.get(0).getStatus());
    }

    @Test
    public void isNotIndex() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertTrue(taskService.isNotIndex(userId, 99));
        Assert.assertFalse(taskService.isNotIndex(userId, 0));
    }

    @Test
    public void bindTaskByProjectIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final Project newProject = new Project();
        newProject.setUserId(userId);
        newProject.setName("new project");
        newProject.setDescription("desc");
        projectService.add(newProject);
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertEquals(projectId, taskList.get(0).getProjectId());
        @NotNull final String newProjectId = newProject.getId();
        projectTaskService.bindTaskByProjectId(userId, newProjectId, taskId);
        Assert.assertEquals(newProjectId, taskList.get(0).getProjectId());
    }

    @Test
    public void unbindTaskByProjectIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertEquals(projectId, taskList.get(0).getProjectId());
        projectTaskService.unbindTaskByProjectId(userId, projectId, taskId);
        Assert.assertNull(taskList.get(0).getProjectId());
    }

    @Test
    public void findTasksByProjectIdTest() {
        @NotNull final List<Task> expectedTaskList = taskList.subList(0,1);
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(expectedTaskList, projectTaskService.findTasksByProjectId(userId, projectId));
    }

    @Test
    public void removeProjectByIdTest() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertEquals(1, projectTaskService.findTasksByProjectId(userId, projectId).size());
        projectTaskService.removeProjectById(projectId);
        Assert.assertEquals(0, projectTaskService.findTasksByProjectId(userId, projectId).size());
    }

}
